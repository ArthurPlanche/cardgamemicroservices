package card.restCrt;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import card.model.Carte;
import card.service.ServiceCarte;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CarteRestCrt.class)
public class TestCarteRestCrt {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private ServiceCarte cService;

	Carte mockCarte=new Carte("Uchiwa Sasuke", 300,8000,"coucou.fr", 300, 400, 6666, "Je n'ai qu'un but, tuer un homme");
	
	@Test
	public void getCarte() throws Exception {
		Mockito.when(
				cService.getCarte(Mockito.anyInt())
				).thenReturn(mockCarte);
				

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/Carte/getCard?idCard=50").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse().getContentAsString());
		String expectedResult="{\"id\":0,\"name\":\"Uchiwa Sasuke\",\"attaque\":300,\"defense\":400,\"prix\":6666,\"imgUrl\":\"coucou.fr\",\"owner\":0,\"description\":\"Je n'ai qu'un but, tuer un homme\",\"hp\":300,\"energy\":8000}";


		JSONAssert.assertEquals(expectedResult, result.getResponse()
				.getContentAsString(), false);
	}
	
	@Test
	public void getList() throws Exception {
		List<Carte> liste = new ArrayList<Carte>();
		liste.add(mockCarte);
		Mockito.when(
				cService.getListCarteUser(Mockito.anyInt())
				).thenReturn(liste);
				

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/Carte/liste?idUser=50").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse().getContentAsString());
		String expectedResult="[{\"id\":0,\"name\":\"Uchiwa Sasuke\",\"attaque\":300,\"defense\":400,\"prix\":6666,\"imgUrl\":\"coucou.fr\",\"owner\":0,\"description\":\"Je n'ai qu'un but, tuer un homme\",\"hp\":300,\"energy\":8000}]";


		JSONAssert.assertEquals(expectedResult, result.getResponse().getContentAsString()
				, false);
	}

}
