package card.model;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestCarte {
	private List<String> stringList;
	private List<Integer> intList;

	@Before
	public void setUp() {
		System.out.println("[BEFORE TEST] -- Add Card to test");
		stringList = new ArrayList<String>();
		intList = new ArrayList<Integer>();
		stringList.add("normalString1");
		stringList.add("normalString2");
		stringList.add(";:!;!::!;;<>");
		intList.add(5);
		intList.add(500);
		intList.add(-1);
	}

	@After
	public void tearDown() {
		System.out.println("[AFTER TEST] -- CLEAN card list");
		stringList = null;
		intList = null;
	}


	@Test
	public void createCarte() {
		for(String msg:stringList) {
			for(Integer msg2:intList) {
				for(Integer msg3:intList) {
					for(String msg4:stringList) {
						for(Integer msg5:intList) {
							for(Integer msg6:intList) {
								for(Integer msg7:intList) {
									for(String msg8:stringList) {
										Carte c=new Carte(msg, msg2, msg3, msg4, msg5, msg6, msg7, msg8);
										System.out.println("msg:"+msg+", msg2:"+msg2+", msg3:"+msg3+", msg4:"+msg4+", msg5:"+msg5+", msg6:"+msg6+", msg7:"+msg7+", msg8:"+msg8);
										assertTrue(c.getImgUrl() == msg4);
										assertTrue(c.getName() == msg);
										assertTrue(c.getHP() == msg2.intValue());
										assertTrue(c.getEnergy() == msg3.intValue());
										assertTrue(c.getAttaque() == msg5.intValue());
										assertTrue(c.getDefense() == msg6.intValue());
										assertTrue(c.getPrix() == msg7.intValue());
										assertTrue(c.getDescription() == msg8);
									}
								}
							}
							
						}
					}	
				}	
			}
		}
	}

	@Test
	public void displayCarte() {
		String url =" https://media-exp1.licdn.com/dms/image/C4E03AQGqCwLK6mad1A/profile-displayphoto-shrink_200_200/0/1615303419962?e=1626307200&v=beta&t=lMoo-9eUfARO7k6OFYhWTQSawKFQXWmjVf0HUSmftNI";
		Carte c = new Carte("ThomasBG", 500,1000,url, 500, 500, 1000000, "Thomas, l'homme le plus chaud de ta région");
		String expectedResult="Card [0]: name:ThomasBG, HP:500, Attaque:500, Defense:500, Energy:1000, prix:1000000 imgUrl: https://media-exp1.licdn.com/dms/image/C4E03AQGqCwLK6mad1A/profile-displayphoto-shrink_200_200/0/1615303419962?e=1626307200&v=beta&t=lMoo-9eUfARO7k6OFYhWTQSawKFQXWmjVf0HUSmftNI, Description:Thomas, l'homme le plus chaud de ta région";
		assertTrue(c.toString().equals(expectedResult));
	}


}
