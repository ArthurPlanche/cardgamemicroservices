package card.repository;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import card.model.Carte;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TestCarteRepository {

	@Autowired
	CarteRepository crepo;
	
	 @Before
     public void setUp() {
		 String url =" https://media-exp1.licdn.com/dms/image/C4E03AQGqCwLK6mad1A/profile-displayphoto-shrink_200_200/0/1615303419962?e=1626307200&v=beta&t=lMoo-9eUfARO7k6OFYhWTQSawKFQXWmjVf0HUSmftNI";
         crepo.save(new Carte("ThomasBG", 500,1000,url, 500, 500, 1000000, "Thomas, l'homme le plus chaud de ta région"));
     }

     @After
     public void cleanUp() {
    	 crepo.deleteAll();
     }

     @Test
     public void saveCard() {
    	 crepo.save(new Carte("ThomasBG", 500,1000,"bonjour.com", 500, 500, 1000000, "Thomas, l'homme le plus chaud de ta région"));
         assertTrue(true);
     }

     @Test
     public void saveAndGetCard() {
    	 crepo.deleteAll();
    	 crepo.save(new Carte("Uchiwa Sasuke", 300,8000,"coucou.fr", 300, 400, 6666, "Je n'ai qu'un but, tuer un homme"));
         List<Carte> carteList = new ArrayList<>();
         crepo.findAll().forEach(carteList::add);
         assertTrue(carteList.size() == 1);
         assertTrue(carteList.get(0).getDescription().equals("Je n'ai qu'un but, tuer un homme"));
         assertTrue(carteList.get(0).getName().equals("Uchiwa Sasuke"));
         assertTrue(carteList.get(0).getImgUrl().equals("coucou.fr"));
         assertTrue(carteList.get(0).getHP()== 300);
         assertTrue(carteList.get(0).getEnergy()== 8000);
         assertTrue(carteList.get(0).getAttaque()== 300);
         assertTrue(carteList.get(0).getDefense()== 400);
         assertTrue(carteList.get(0).getPrix()== 6666);
     }

     @Test
     public void getCard() {
         List<Carte> carteList = crepo.findByName("ThomasBG");
         assertTrue(carteList.size() == 1);
         assertTrue(carteList.get(0).getName().equals("ThomasBG"));
         assertTrue(carteList.get(0).getDescription().equals("Thomas, l'homme le plus chaud de ta région"));
         assertTrue(carteList.get(0).getImgUrl().equals(" https://media-exp1.licdn.com/dms/image/C4E03AQGqCwLK6mad1A/profile-displayphoto-shrink_200_200/0/1615303419962?e=1626307200&v=beta&t=lMoo-9eUfARO7k6OFYhWTQSawKFQXWmjVf0HUSmftNI"));
         assertTrue(carteList.get(0).getHP()== 500);
         assertTrue(carteList.get(0).getEnergy()== 1000);
         assertTrue(carteList.get(0).getAttaque()== 500);
         assertTrue(carteList.get(0).getDefense()== 500);
         assertTrue(carteList.get(0).getPrix()== 1000000);
     }

     @Test
     public void findByName() {
    	 crepo.save(new Carte("Uchiwa Sasuke", 300,8000,"coucou.fr", 300, 400, 6666, "Je n'ai qu'un but, tuer un homme"));
    	 crepo.save(new Carte("Patrick", 500,1000,"bonjour.com", 500, 500, 1000000, "Thomas, l'homme le plus chaud de ta région"));
    	 crepo.save(new Carte("Patrick", 500,1000,"aurevoir.com", 500, 500, 1000000, "Thomas, l'homme le plus chaud de ta région"));
         List<Carte> carteList = new ArrayList<>();
         crepo.findByName("Patrick").forEach(carteList::add);
         assertTrue(carteList.size() == 2);
     }
     
     @Test
     public void findByOwnerId() {
    	 String url =" https://media-exp1.licdn.com/dms/image/C4E03AQGqCwLK6mad1A/profile-displayphoto-shrink_200_200/0/1615303419962?e=1626307200&v=beta&t=lMoo-9eUfARO7k6OFYhWTQSawKFQXWmjVf0HUSmftNI";
 		 Carte c = new Carte("ThomasBG", 500,1000,url, 500, 500, 1000000, "Thomas, l'homme le plus chaud de ta région");
         c.setOwner(1);
         crepo.save(c);
        
         String url2 ="https://www.pokepedia.fr/images/thumb/6/66/Pingol%C3%A9on-DP.png/1200px-Pingol%C3%A9on-DP.png";
 		 Carte c2 = new Carte("Pingoleon", 100,500,url2, 75, 100, 250, "Pingoleon est surnomme frequemment l'Empereur des glaces en raison de son apparence royale. S'il est le chef d'une tribu, son trident sera plus gros.");
         c2.setOwner(2);
         crepo.save(c2);
         
         String url3 =" https://pbs.twimg.com/profile_images/1220452744579948544/SXcvaAYp_400x400.jpg";
         Carte c3 = new Carte("Uchiwa Sasuke", 300,8000,url3, 300, 400, 6666, "Je n'ai qu'un but, tuer un homme");
         c3.setOwner(1);
         crepo.save(c3);  
    	 
         List<Carte> carteList = new ArrayList<>();
         crepo.findByOwnerId(1).forEach(carteList::add);
         assertTrue(carteList.size() == 2);
     }

}
