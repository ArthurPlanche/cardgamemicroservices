package card.service;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import card.model.Carte;
import card.repository.CarteRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ServiceCarte.class)
public class TestCarteService {

	@Autowired
	private ServiceCarte cService;

	@MockBean
	private CarteRepository cRepo;
	
	Carte tmpCarte=new Carte("Uchiwa Sasuke", 300,8000,"coucou.fr", 300, 400, 6666, "Je n'ai qu'un but, tuer un homme");
	
	@Test
	public void getCarte() {
		Mockito.when(
				cRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpCarte));
		Carte heroInfo=cService.getCarte(45);
		assertTrue(heroInfo.toString().equals(tmpCarte.toString()));
	}
	
	@Test
	public void changeOwner() {
		tmpCarte.setOwner(2);
		Mockito.when(
				cRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpCarte));
		cService.changeOwner(1, 7);
		assertTrue(tmpCarte.getOwner() == 1);
	}
	
	@Test
	public void setPrix() {
		Mockito.when(
				cRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpCarte));
		cService.setPrix(5, 123456);
		assertTrue(tmpCarte.getPrix() == 123456);
	}
	
}