package card.restCrt;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import card.model.Carte;
import card.service.ServiceCarte;

@RestController
@RequestMapping("/Carte")
public class CarteRestCrt {
	
	@Autowired
	ServiceCarte cService;
	
	/**
	 * 
	 * @param idCarte
	 * @return la carte correspondante à l'id récupéré dans l'url
	 */
	@RequestMapping("/getCard")
	public Carte getCarte(@RequestParam int idCard) {
		return cService.getCarte(idCard);
	}
	
	/**
	 * 
	 * @param idUser
	 * @return la liste des cartes appartenant à l'utilisateur dont l'id est passé en paramètre dans l'url
	 */
	@RequestMapping("/liste")
	public List<Carte> getList(@RequestParam int idUser){
		List<Carte> liste = cService.getListCarteUser(idUser);
		return liste;
	}
	
	/**
	 * 
	 * @param idUser
	 * @param idCard
	 * change l'owner de la carte dont l'id est en paramètre dans l'url, le nouvel owner est l'utilisateur dont l'id est
	 * lui aussi en paramètre dans l'url
	 */
	@RequestMapping("/changeOwner")
	public void changeOwner(@RequestParam int idUser, @RequestParam int idCard) {
		cService.changeOwner(idUser, idCard);
	}
	
	/**
	 * 
	 * @param idCard
	 * @param prix
	 * 
	 * change le prix d'une carte lorsque l'utilisateur la met sur le market pour la vendre
	 */
	@RequestMapping("/setPrix")
	public void setPrix(@RequestParam int idCard, @RequestParam int prix) {
		cService.setPrix(idCard, prix);
	}

}
