package card.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import card.model.Carte;
import card.repository.CarteRepository;

@Service
public class ServiceCarte {
	@Autowired
	CarteRepository cRepository;
	
	public void addCarte(Carte c) {
		Carte createdCarte=cRepository.save(c);
		System.out.println(createdCarte);
	}

	public Carte getCarte(int id) {
		Optional<Carte> cOpt =cRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}
	
	public List<Carte> getAllCartes(){
		Iterable<Carte> listeCarteIterable = cRepository.findAll();
		List<Carte> listeCarte = new ArrayList<Carte>();
		listeCarteIterable.forEach(listeCarte::add);
		return listeCarte;
	}
	
	public List<Carte> getListCarteUser(int idUser){
		List<Carte> Liste = cRepository.findByOwnerId(idUser);
		return Liste;
	}
	
	public void changeOwner(int idUser, int idCard) {
		Carte carte = getCarte(idCard);
		carte.setOwner(idUser);
		cRepository.save(carte);
	}
	
	public void setPrix(int idCard, int prix) {
		Carte carte = getCarte(idCard);
		carte.setPrix(prix);
		cRepository.save(carte);
	}
}