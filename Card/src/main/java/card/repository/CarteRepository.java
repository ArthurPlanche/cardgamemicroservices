package card.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import card.model.Carte;

public interface CarteRepository extends CrudRepository<Carte, Integer> {

	public List<Carte> findByName(String name);
	public List<Carte> findByOwnerId(int ownerId);
	
}