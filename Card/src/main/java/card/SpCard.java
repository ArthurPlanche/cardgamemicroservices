package card;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpCard {
	
	public static void main(String[] args) {
		SpringApplication.run(SpCard.class,args);
	}
}