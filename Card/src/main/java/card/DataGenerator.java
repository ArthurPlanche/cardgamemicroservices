package card;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import card.model.Carte;
import card.service.ServiceCarte;

@Component
public class DataGenerator implements ApplicationRunner {
	
	@Autowired
	ServiceCarte cService;
	
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		String url =" https://media-exp1.licdn.com/dms/image/C4E03AQGqCwLK6mad1A/profile-displayphoto-shrink_200_200/0/1615303419962?e=1626307200&v=beta&t=lMoo-9eUfARO7k6OFYhWTQSawKFQXWmjVf0HUSmftNI";
		Carte c = new Carte("ThomasBG", 500,1000,url, 500, 500, 1000000, "Thomas, l'homme le plus chaud de ta région");
        c.setOwner(1);
		cService.addCarte(c);
       
        String url2 ="https://www.pokepedia.fr/images/thumb/6/66/Pingol%C3%A9on-DP.png/1200px-Pingol%C3%A9on-DP.png";
		Carte c2 = new Carte("Pingoleon", 100,500,url2, 75, 100, 250, "Pingoleon est surnomme frequemment l'Empereur des glaces en raison de son apparence royale. S'il est le chef d'une tribu, son trident sera plus gros.");
        c2.setOwner(2);
		cService.addCarte(c2);
        
        String url3 =" https://pbs.twimg.com/profile_images/1220452744579948544/SXcvaAYp_400x400.jpg";
        Carte c3 = new Carte("Uchiwa Sasuke", 300,8000,url3, 300, 400, 6666, "Je n'ai qu'un but, tuer un homme");
        c3.setOwner(1);
        cService.addCarte(c3);  
	}


}
