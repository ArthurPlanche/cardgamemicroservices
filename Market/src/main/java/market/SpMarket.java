package market;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpMarket {
	
	public static void main(String[] args) {
		SpringApplication.run(SpMarket.class,args);
	}
}