package market.restCrt;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import market.service.MarketServiceMS;



@RestController
@RequestMapping("/Market")

public class MarketRestCrtMS {
	@Autowired
    MarketServiceMS mService;

	public static class sellcardDTO {
		private int idcard;
		private int prix;
		private int iduser;
		
		public sellcardDTO() {}
		public int getIdcard() {
			return idcard;
		}
		public int getPrix() {
			return prix;
		}
		public int getIduser() {
			return iduser;
		}
		
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/sellcard") 
	public void sellCard(@RequestBody sellcardDTO sellcard)  { 
		mService.sellCard(sellcard.getIduser(), sellcard.getIdcard(), sellcard.getPrix()); 
	}

	@RequestMapping(method=RequestMethod.GET,value="/buy")
	public void buyCard(@RequestParam String idcard, @RequestParam String idseller, @RequestParam String idbuyer) {
        mService.buyCard(Integer.valueOf(idcard),Integer.valueOf(idseller),Integer.valueOf(idbuyer));       
    }

	@GetMapping("/testcard")
    public String test(Model model,@RequestParam int idUser) {

		List<Integer> listeCard = mService.getListCard();
        model.addAttribute("listeCarte",listeCard);
        model.addAttribute("idUser", idUser);
        
        return "cardmarketlist"; // Non utilise pour le moment (retour au template , qui etait utile lors de la 1ere validation (atelier 2)
    }
	
}