package market.service;

import java.util.ArrayList;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonProperty;







@Service
public class MarketServiceMS {
	
	String url_user = "http://localhost:8081/User";
	String url_carte = "http://localhost:8081/Carte";
	

//	List<Carte> Cardtosell = new ArrayList<Carte>(); // Liste de Carte à vendre, qu'on affichera sur la page Market
	
	private ArrayList<Integer> ListCard = new ArrayList<Integer>();
	
	
	
	public ArrayList<Integer> getListCard() {
        return ListCard;
    }
	
	 public void AddCardToSell(int id) {
	        ListCard.add(id);
	 }
	 



	 
	 
	 
	 
	 public static class CarteDTO {
		 private int prix;
		 private int ownerId;
		 //Si jamais on a un autre truc dans notre JSON de USER (genre un PASSWD pa rexemple), on rajoute prinvate string passwd, on fait les getter/setter et on l'utilise en dessous pareil que pr money (peu importe ou iul est dans le json, donc cette fonction est broken)

		public CarteDTO() {}
		public int getPrix() {
			return prix;
		}

		public void setMoney(int prix_carte) {
			this.prix = prix;
		}
		public int getOwner() {
			return ownerId;
		}
		 
	 }
	 
	 
	 
	 
	 
	 public static class UserDTO {
		 @JsonProperty("argent")
		 private int money;
		 
		 //Si jamais on a un autre truc dans notre JSON de USER (genre un PASSWD par exemple), on rajoute private string passwd, on fait les getter/setter et on l'utilise en dessous pareil que pr money (peu importe ou iul est dans le json, donc cette fonction est broken)

		public UserDTO() {}
		public int getMoney() {
			return money;
		}

		public void setMoney(int money) {
			this.money = money;
		}
		 
	 }
	 
	 
	 
	 

	 
		
	public void buyCard(int idcard, int idseller, int idbuyer) { 

		
		ResponseEntity<CarteDTO> carte = new RestTemplate().getForEntity(url_carte + "/getCard?idCard=" + String.valueOf(idcard), CarteDTO.class);
	    ResponseEntity<UserDTO> user_buyer = new RestTemplate().getForEntity(url_user + "/userId/" + String.valueOf(idbuyer), UserDTO.class); // recupere l'user       
	    ResponseEntity<UserDTO> user_seller = new RestTemplate().getForEntity(url_user + "/userId/" + String.valueOf(idseller), UserDTO.class);
	    
	    
	    if (carte != null) {
	    //	if (carte.getBody().ownerId == idseller) {
	    		if (user_buyer.getBody().money >= carte.getBody().prix) {
	    	
		    new RestTemplate().exchange(url_user + "/suppM?id=" + String.valueOf(idbuyer) + "&money=" + carte.getBody().prix, HttpMethod.PUT, null, Void.class); // Supprime l'argent de l'acheteur du a l'achat
		    ResponseEntity<CarteDTO> ajouter_carte = new RestTemplate().getForEntity(url_carte + "/changeOwner?idUser=" + String.valueOf(idbuyer) + "&idCard=" + String.valueOf(idcard), CarteDTO.class);	// Ajoute la carte a la liste des cartes du buyer et supprime a celle du seller
		    new RestTemplate().exchange(url_user + "/addM?id=" + String.valueOf(idseller) + "&money=" + carte.getBody().prix, HttpMethod.PUT, null, Void.class); // Ajoute l'argent au seller
	//    		}    
	    	}
	    }
	    }  
    	  	                                
	    
	
				

	
	
	
	
	

	public void sellCard(int idUser, int idCard, int prix) //On part du principe que tant qu'une carte n'est pas 'achete, meme si elle est sur le marche, l'utilisateur peut toujours l'utiliser (comme sur E-bay)
	// de plus, cette fonction sert a 'set' le prix d'une carte selon le bon vouloir du propriétaire, et uniquement ça, puisque l'affichage sur la liste des cartes du market (derniere ligne de la fonction) serait utile pour le frontend, que nous faisons pas apparaitre ici
	{
		ResponseEntity<CarteDTO> carte = new RestTemplate().getForEntity(url_carte + "/getCard?idCard=" + String.valueOf(idCard), CarteDTO.class);	// recupere la carte
	    ResponseEntity<UserDTO> user_seller = new RestTemplate().getForEntity(url_user + "/userId/" + String.valueOf(idUser), UserDTO.class); // recupere l'user
    	
//	    if (carte.getBody().ownerId == idUser) 
	    
	    // ON NE SE SERT PAS DE CETTE CONDITION CAR DANS LE FRONTEND LE BOUTON 'VENDRE' NE SERA DISPONIBLE 
	    //QUE DANS LA LISTE DES CARTES DE L'UTILISATEUR, DONC IL N'Y AURA AUCUN PROBLEME : 
	    //IL NE POURRA PAS VENDRE LES CARTES D'UN AUTRE USER PUISQU'IL N'AURA MEME PAS LE BOUTON VENDRE AFFICHER (coté front)
	 
	    ResponseEntity<CarteDTO> set_carte = new RestTemplate().getForEntity(url_carte + "/setPrix?idCard=" + String.valueOf(idCard) + "&prix=" + String.valueOf(prix), CarteDTO.class); // set le nouveau prix de la carte
		AddCardToSell(idCard);	// l'ajoute a la liste des cartes dispo sur le marché (pour le front end uniquement or ici on ne s'en occupe pas)

		
	//	ResponseEntity<CarteDTO> supprimer_carte = new RestTemplate().getForEntity(url_carte + "/delcard?idUser=" + String.valueOf(idUser) + "&idCard=" + String.valueOf(idCard), CarteDTO.class);	
	//  SI JAMAIS on voulait que l'utilisateur ne puisse plus se servir de la carte, une fois qu'il la mis en vente				

	}
	
}
