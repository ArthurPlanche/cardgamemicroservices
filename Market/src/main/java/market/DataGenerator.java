package market;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import market.service.MarketServiceMS;

// Data Generator pour test fonctionnels sous postman
@Component
public class DataGenerator implements ApplicationRunner {
	
	@Autowired
	MarketServiceMS mService;
	
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		int id=1;
        mService.AddCardToSell(id);  
	}


}
