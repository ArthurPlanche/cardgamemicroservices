package market.service;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import card.model.Carte;
import card.repository.CarteRepository;
import market.service.MarketServiceMS.CarteDTO;
import market.service.MarketServiceMS.UserDTO;





@RunWith(SpringRunner.class)
@WebMvcTest(value = MarketServiceMS.class)
public class TestMarketService {

	@Autowired
	private MarketServiceMS mService;

	@MockBean

	public void JsonMethod() {
	JSONObject actual = new JSONObject();
	actual.put("Name", "Uchiwa Sasuke");
	actual.put("HP",300);
    actual.put("Energy",8000);
    actual.put("imgUrl", "coucou.fr");
    actual.put("attaque", 300);
    actual.put("defense", 400);
    actual.put("prix", 6666);
    actual.put("description", "Je n'ai qu'un but, tuer un homme");
	}

	
	
	

	
	
	// DIFFERENTS TESTS NON FONCTIONNELS, 
	@Test
	public void buyCard() {
		Mockito.when(
				cRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpCarte));
		Carte heroInfo=cService.getCarte(45);
		assertTrue(heroInfo.toString().equals(tmpCarte.toString()));
	}
	

	// DIFFERENTS TESTS NON FONCTIONNELS, 
	@Test
	public void changeOwner() {
		tmpCarte.setOwner(2);
		Mockito.when(
				cRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpCarte));
		cService.changeOwner(1, 7);
		assertTrue(tmpCarte.getOwner() == 1);
	}
	

}