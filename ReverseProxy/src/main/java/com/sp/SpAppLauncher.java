package com.sp;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class SpAppLauncher {

	public static void main(String[] args) {
		SpringApplication.run(SpAppLauncher.class,args);
	}

}
