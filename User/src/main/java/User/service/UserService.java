package User.service;

import java.util.ArrayList;
//import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import User.model.User;
import User.repository.UserRepository;


@Service

public class UserService {
	
	@Autowired
	UserRepository uRepository;

	public User findUser(String username) {
		 Optional<User> user = uRepository.findByUsername(username);
		if (user.isPresent()) {
			return user.get();
		}else {
			return null;
		}	
	}
	
	public User findUserById(int id) {
		 Optional<User> user = uRepository.findById(id);
		if (user.isPresent()) {
			return user.get();
		}else {
			return null;
		}	
	}
	
	public boolean CreateUser(String FirstName, String LastName,String UserName, String passwd,int argent) {
		//test condition si login est vide
		if (UserName.isEmpty()) {
			return false;
		}
		else {
			User user = new User (FirstName,LastName, UserName,passwd, argent);
			uRepository.save(user);
			return true;
		}
	}
	
	public boolean CreateUserByUser(User user) {
		//test condition si login est vide
		if (user != null) {
			Optional<User> userExist = uRepository.findByUsername(user.getUserName());
			if (userExist.isPresent()) {
				return false;
			}else {
				uRepository.save(user);
			}	
			return true;
		}
		return false;
	}
	
	public void AddArgent(int idUser, int Argent) {
		Optional<User> user = uRepository.findById(idUser);
		if(user.isPresent()) {
			int argent=user.get().getArgent();
			int somme = Argent + argent;
			user.get().setArgent(somme);
			uRepository.save(user.get());
		}
		
	}
	
	public void SupprimerArgent(int idUser, int Argent) {
		Optional<User> user = uRepository.findById(idUser);
		if(user.isPresent()) {
			int argent=user.get().getArgent();
			int difference = argent - Argent;
			user.get().setArgent(difference);
			uRepository.save(user.get());
		}	
	}
	
	
	
	public int getArgentService(int id) {
		 Optional<User> user = uRepository.findById(id);
		 if(user.isPresent()) {
			 return user.get().getArgent();
		 }
		 else {
			 return -1;
		 }
	}
	//thomas tu esvraiment nul
}