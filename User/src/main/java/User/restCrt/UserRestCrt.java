package User.restCrt;

//import java.util.ArrayList;
//import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import User.model.User;

import User.service.UserService;

@RestController
@RequestMapping("/User")
public class UserRestCrt {
  
  @Autowired
  UserService uService;
  
  @RequestMapping(method=RequestMethod.GET,value="/userId/{id}")
  public User findUserById(@PathVariable int id) {
      User user = uService.findUserById(id);
      return user;
  }
  
  @RequestMapping(method=RequestMethod.GET,value="/username/{UN}")
  public User findUser(@PathVariable String UN) {
      User user = uService.findUser(UN);
      return user;
  }
  
  @RequestMapping(method=RequestMethod.PUT,value="/addM")
  public void addMoney(@RequestParam int id, @RequestParam int money) {
      uService.AddArgent(id, money);
  }
  
  @RequestMapping(method=RequestMethod.PUT,value="/suppM")
  public void suppMoney(@RequestParam int id, @RequestParam int money) {
      uService.SupprimerArgent(id, money);
  }

  @RequestMapping(method=RequestMethod.POST,value="/createUser")
  public boolean createUser(@RequestBody User user) {
      boolean ret = uService.CreateUserByUser(user);
      return ret;
  }


}