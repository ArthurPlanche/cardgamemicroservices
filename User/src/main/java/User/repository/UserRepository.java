package User.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import User.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

    public Optional<User> findByUsername(String username);
}