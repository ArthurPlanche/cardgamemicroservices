package User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import User.model.User;
import User.service.UserService;

@Component
public class DataGenerator implements ApplicationRunner {
	
	@Autowired
	UserService uService;
		
	@Override
	public void run(ApplicationArguments args) throws Exception {
		uService.CreateUser("Oscar", "Chicote", "oscar", "cn", 3000);  
		uService.CreateUser("Arthur", "planche", "helo", "wol", 3); 
		uService.CreateUser("Thomas", "Scaca", "yo", "bg", 69); 
	}


}
