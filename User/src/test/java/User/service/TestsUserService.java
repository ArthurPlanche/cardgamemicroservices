package User.service;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import User.model.User;
import User.repository.UserRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserService.class)
public class TestsUserService {


	@Autowired
	private UserService uService;

	@MockBean
	private UserRepository uRepo;
	
	User tmpUser = new User("Oscar", "Chicote", "oscar", "cn", 9999);
	
	@Test
	public void GetArgent() {
		Mockito.when(
				uRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpUser));
		assertTrue(uService.getArgentService(45) == tmpUser.getArgent());
	}
	
	@Test
	public void AddArgent() {
		Mockito.when(
				uRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpUser));
		uService.AddArgent(45,1);
		assertTrue(uService.getArgentService(45) == 10000);
	}
	
	@Test
	public void SupprimerArgent() {
		Mockito.when(
				uRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpUser));
		uService.SupprimerArgent(45,1);
		assertTrue(uService.getArgentService(45) == 9998);
	}

}
