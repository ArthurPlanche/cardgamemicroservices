package User.model;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestUserModel {

	private List<String> stringList;
	private List<Integer> intList;

	@Before
	public void setUp() {
		System.out.println("[BEFORE TEST] -- Add user to test");
		stringList = new ArrayList<String>();
		intList = new ArrayList<Integer>();
		stringList.add("normalString1");
		stringList.add("normalString2");
		stringList.add(";:!;!::!;;<>");
		intList.add(5);
		intList.add(500);
		intList.add(-1);
	}

	@After
	public void tearDown() {
		System.out.println("[AFTER TEST] -- CLEAN user list");
		stringList = null;
		intList = null;
	}


	@Test
	public void createUser() {
		for(String msg:stringList) {
			for(String msg2:stringList) {
				for(String msg3:stringList) {
					for(String msg4:stringList) {
						for(Integer msg5:intList) {
							User u=new User(msg, msg2, msg3, msg4, msg5);
							System.out.println("msg:"+msg+", msg2:"+msg2+", msg3:"+msg3+", msg4:"+msg4+", msg5:"+msg5);
							assertTrue(u.getFirstName() == msg);
							assertTrue(u.getLastName() == msg2);
							assertTrue(u.getUserName() == msg3);
							assertTrue(u.getPasswd() == msg4);
							assertTrue(u.getArgent() == msg5.intValue());
						}
					}	
				}	
			}
		}
	}

	@Test
	public void displayUser() {
		User u = new User("Oscar", "Chicote", "oscar", "cn", 9999);
		String expectedResult="User: 0 prénom: Oscar nom: Chicote nombre de carte: ";
		assertTrue(u.toString().equals(expectedResult));
	}



}
