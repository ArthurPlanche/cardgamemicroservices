package User.restCrt;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import User.model.User;
import User.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserRestCrt.class)
public class TestUserRestCrt {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private UserService uService;

	User mockUser = new User("Oscar", "Chicote", "oscar", "cn", 9999);
	
	@Test
	public void userId() throws Exception {
		Mockito.when(
				uService.findUserById(Mockito.anyInt())
				).thenReturn(mockUser);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/User/userId/50").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse().getContentAsString());
		String expectedResult="{\"id\":0,\"passwd\":\"cn\",\"argent\":9999,\"userName\":\"oscar\",\"firstName\":\"Oscar\",\"lastName\":\"Chicote\"}";


		JSONAssert.assertEquals(expectedResult, result.getResponse()
				.getContentAsString(), false);
	}
	
	@Test
	public void username() throws Exception {
		Mockito.when(
				uService.findUser(Mockito.anyString())
				).thenReturn(mockUser);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/User/username/oscar").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse().getContentAsString());
		String expectedResult="{\"id\":0,\"passwd\":\"cn\",\"argent\":9999,\"userName\":\"oscar\",\"firstName\":\"Oscar\",\"lastName\":\"Chicote\"}";


		JSONAssert.assertEquals(expectedResult, result.getResponse()
				.getContentAsString(), false);
	}
	


}
