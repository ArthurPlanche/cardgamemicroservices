package User.repository;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import User.model.User;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TestUserRepository {

	@Autowired
	UserRepository urepo;
	
	 @Before
     public void setUp() {
		 urepo.save(new User("Oscar", "Chicote", "oscar", "cn", 9999));
     }

     @After
     public void cleanUp() {
    	 urepo.deleteAll();
     }

     @Test
     public void saveUser() {
    	 urepo.save(new User("Arthur", "planche", "helo", "wol", 3));
         assertTrue(true);
     }

     @Test
     public void saveAndGetUser() {
    	 urepo.deleteAll();
    	 urepo.save(new User("Arthur", "planche", "helo", "wol", 3));
         List<User> userList = new ArrayList<>();
         urepo.findAll().forEach(userList::add);
         assertTrue(userList.size() == 1);
         assertTrue(userList.get(0).getFirstName().equals("Arthur"));
         assertTrue(userList.get(0).getLastName().equals("planche"));
         assertTrue(userList.get(0).getUserName().equals("helo"));
         assertTrue(userList.get(0).getPasswd().equals("wol"));
         assertTrue(userList.get(0).getArgent() == 3);
     }

     @Test
     public void findByUsername() {
         Optional<User> userList = urepo.findByUsername("oscar");
         assertTrue(userList.isPresent() == true);
         assertTrue(userList.get().getFirstName().equals("Oscar"));
         assertTrue(userList.get().getLastName().equals("Chicote"));
         assertTrue(userList.get().getUserName().equals("oscar"));
         assertTrue(userList.get().getPasswd().equals("cn"));
         assertTrue(userList.get().getArgent() == 9999);
     }

}
