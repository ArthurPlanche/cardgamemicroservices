package com.sp.room.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.game.card.Game;
import com.sp.room.model.Room;
import com.sp.room.repository.RoomRepository;

@Service
public class RoomService {
	@Autowired
	RoomRepository rRepository;

	public void addRoom(int userID, float bet) {
		rRepository.save(new Room(userID, bet));
	}

	public boolean joinRoom(int organizerID, int joinerID) {
		Optional<Room> cOpt = rRepository.findById(organizerID);
		if (cOpt.isPresent()) {
			cOpt.get().setJoinerID(joinerID);
			System.out.print(cOpt.get().toString());
			rRepository.save(cOpt.get());
			return true;
		}
		return false;
	}
	
	
	public Room getRoom(int organizerID) {
		Optional<Room> cOpt = rRepository.findById(organizerID);
		if (cOpt.isPresent()) 
			return cOpt.get();
		
		return null;
	}

	public boolean addCard(int organizerID, int cardID, int userID) {
		Optional<Room> cOpt = rRepository.findById(organizerID);
		if (cOpt.isPresent()) {
			if(organizerID == userID && cOpt.get().getOrganizerCardID() != Room.DEFAULT_ID()) {
				cOpt.get().setJoinerCardID(cardID);
				rRepository.save(cOpt.get());
				return true;
			}
			else if(cOpt.get().getJoinerID() == userID && cOpt.get().getJoinerCardID() != Room.DEFAULT_ID()) {
				cOpt.get().setOrganizerCardID(cardID);
				rRepository.save(cOpt.get());
				return true;
			}
		}
		return false;
	}
}
