package com.sp.room.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;


@Entity
public class Room {
	@Id
	@NotNull
	@Column(name = "organizerID")
	private int organizerID;
	@NotNull
	private int organizerCardID;
	@NotNull
	private int joinerID;
	@NotNull
	@Column(name = "joinerCardID")
	private int joinerCardID;
	@NotNull
	private int winnerID;
	@NotNull
	float organizerBet;
	
	public Room() {
	}
	
	public Room(int organizerID, float organizerBet) {
		this.organizerID = organizerID;
		this.organizerBet = organizerBet;
		this.setJoinerID(Room.DEFAULT_ID());
		this.setWinnerID(Room.DEFAULT_ID());
		this.setJoinerCardID(Room.DEFAULT_ID());
		this.setOrganizerCardID(Room.DEFAULT_ID());
	}
	
	public int getOrganizerID() {
		return this.organizerID;
	}
	
	public int getJoinerID() {
		return this.joinerID;
	}
	
	public void setOrganizerCardID(int id) {
		this.organizerCardID = id;
	}
	
	public void setJoinerCardID(int id) {
		this.joinerCardID = id;
	}
	
	public int getOrganizerCardID() {
		return this.organizerCardID;
	}
	
	public int getJoinerCardID() {
		return this.joinerCardID;
	}
	
	public float getOrganizerBet() {
		return this.organizerBet;
	}
	
	public void setOrganizerBet(float bet) {
		this.organizerBet = bet;
	}
	
	public int getWinnerID() {
		return this.winnerID;
	}
	
	public void setWinnerID(int id) {
		this.winnerID = id;
	}
	
	public void setJoinerID(int id) {
		this.joinerID = id;
	}
	
	public void setOrganizerID(int id) {
		this.organizerID = id;
	}
	
	
	public static int DEFAULT_ID() {
		return -1;
	}

	@Override
	public String toString() {
		return "Room : organizerID:"+this.organizerID+", joiner:"+this.joinerID;
	}
	 
}
