package com.sp.room;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpRoom {
	public static void main(String[] args) {
		SpringApplication.run(SpRoom.class,args);
	}
}
