package com.sp.room.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.sp.game.rest.GameRestCrl;
import com.sp.room.model.Room;
import com.sp.room.service.RoomService;

@RestController
public class RoomRestCrt {
    @Autowired
    RoomService roomService;
    //@Autowired
    //GameRestCrl game;
    
    @RequestMapping(method=RequestMethod.PUT,value="/Room/CreateRoom/{organizerID}&{bet}")
    public int createRoom(@PathVariable int organizerID, @PathVariable float bet) {
    	roomService.addRoom(organizerID, bet);
    	//game.initGame(organizerID);
    	throw new ResponseStatusException(HttpStatus.OK, "OK");
    }
    
    @RequestMapping(method=RequestMethod.GET,value="/Room/GetRoom/{organizerID}")
    public Room getRoom(@PathVariable int organizerID) {
    	Room room = roomService.getRoom(organizerID);
    	if(room != null) 
    		return room;
    	throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not Found");
    }
    
    @RequestMapping(method=RequestMethod.POST,value="/Room/JoinRoom/{organizerID}&{joinerID}")
    public void joinRoom(@PathVariable int organizerID, @PathVariable int joinerID) {
    	if(roomService.joinRoom(organizerID, joinerID)) 
    		throw new ResponseStatusException(HttpStatus.OK, "OK");
    	else 
    		throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Arguments");
    }
    
    @RequestMapping(method=RequestMethod.PUT,value="/Room/AddCard/{organizerID}&{cardID}&{userID}")
    public void addCard(@PathVariable int organizerID, @PathVariable int cardID, @PathVariable int userID) {
    	if(true && roomService.addCard(organizerID, cardID, userID)) 
    		throw new ResponseStatusException(HttpStatus.OK, "OK");
    	else 
    		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Invalid Arguments");
    }
    
   /* @RequestMapping(method=RequestMethod.POST,value="/Room/StartGame/{organizerID}")
    public int StartGame(@PathVariable int organizerID) {
    	if(roomService.getRoom(organizerID) != null) {
    		return game.startGame(organizerID, organizerID);
    	}
    	throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Invalid Arguments");
    }*/
    
}
