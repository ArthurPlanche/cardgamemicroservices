package com.sp.room.repository;

import org.springframework.data.repository.CrudRepository;

import com.sp.room.model.Room;

public interface RoomRepository extends CrudRepository<Room, Integer> {}
