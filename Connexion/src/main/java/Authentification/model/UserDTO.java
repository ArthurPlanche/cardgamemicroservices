package Authentification.model;

public class UserDTO {
	
	private String firstName;
	private String lastName;
	private String userName;
	private String passwd;
	private int argent;

	public UserDTO() {}
	public String getPasswd() {
		return passwd;
	}
	public String getUserName() {
		return userName;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public int getArgent() {
		return argent;
	}
 
}