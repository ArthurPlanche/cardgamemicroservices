package Authentification.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import Authentification.model.UserDTO;


@Service
public class AuthService {
	
	String url_user = "http://localhost:8081/User";
	
	public boolean login (String username, String password) {
		boolean ret = false;
		RestTemplate restTemplate = new RestTemplate();
	    UserDTO user = restTemplate.getForObject(url_user + "/username/" + username, UserDTO.class);
	    if (user != null) {
			ret = password.equals(user.getPasswd());
	    }
		return ret;
	}
	
	public boolean newUser (UserDTO user) {
		boolean ret;
		RestTemplate restTemplate = new RestTemplate();
	    ret = restTemplate.postForObject(url_user + "/createUser", user,  boolean.class);
		return ret;
	}
	
//	public boolean newUser (String username, String pwd, String firstName, String lastName, int money) {
//		boolean ret = false;
//		RestTemplate restTemplate = new RestTemplate();
//	    restTemplate.put(url_user + "/createUser/" + username, void.class);
//		return ret;
//	}
	
//	public int getId(String username) {
//		return uService.findUser(username).getId();
//	}

}