package Authentification.restCrt;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import Authentification.service.AuthService;
import Authentification.model.UserDTO;

@RestController
@RequestMapping("/Auth")
public class AuthRestCrt {
	
	@Autowired
    AuthService AService;
	
	public static class loginDTO {
		private String password;
		private String username;

		public loginDTO() {}
		public String getPassword() {
			return password;
		}
		public String getUsername() {
			return username;
		}
	 }
	
//	@GetMapping("/")
//	public void firstPage(HttpServletResponse response) throws IOException {
//		UService.CreateUser("Arthur", "Planche", "Marchombre12", "yolo", 1500);
//		response.sendRedirect("auth.html");
//	}
	
	@RequestMapping(method=RequestMethod.POST,value="/login")
	public boolean login(@RequestBody loginDTO login) {
		boolean ret = AService.login(login.getUsername(), login.getPassword());
		return ret;
//		if (ret) {
//			response.sendRedirect("/cardList.html?idUser="+AService.getId(username));
//		}
//		else {
//			response.sendRedirect("auth.html");
//		}
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/newUser")
	public boolean newUser(@RequestBody UserDTO user) {
		boolean ret = AService.newUser(user);
		return ret;
	}

}